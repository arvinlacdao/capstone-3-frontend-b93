import React, { useContext, useState, useEffect } from 'react';
import Head from 'next/head';
import { Button, Table, Form } from 'react-bootstrap';
import Course from '../../components/Course';
import coursesData from '../../data/courses';
import UserContext from '../../UserContext';

export default function index() {

	const { user } = useContext(UserContext);
	console.log(coursesData);

	const [courses, setCourses] = useState(coursesData);
	const [term, setTerm] = useState('');
	const [coursesList, setCoursesList] = useState([]);

	// Table rows for admin view
	const coursesRows = coursesData.map(course => {
		return(
			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>Php {course.price}</td>
				<td>{course.onOffer ? 'Open' : 'Closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Button variant="warning">Update</Button>
					<Button variant="danger" className="ml-3">Disable</Button>
				</td>
			</tr>
		)
	})

	useEffect(() => {

		if (term !== '') {
			
			let updatedCourses = courses.map((course) => {
				console.log(course.name.toLowerCase().includes(term.toLowerCase()))
				if (course.name.toLowerCase().includes(term.toLowerCase())){
					return <Course key={course.id} course={course}/>
				} else {
					return null
				}
			})

			setCoursesList(updatedCourses);

		} else {

			const updatedCourses = courses.map(course => {
				if(course.onOffer){
					return <Course key={course.id} course={course}/>
				} else {
					return null;
				}
			})

			setCoursesList(updatedCourses);

		}

	}, [term])

	return (
		user.isAdmin === true ?
			<React.Fragment>
				<Head>
					<title>Courses Admin Dashboard</title>
				</Head>
				<h1>Course Dashboard</h1>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Price</th>
							<th>Status</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{coursesRows}
					</tbody>
				</Table>
			</React.Fragment>
			:
			<React.Fragment>
				<Head>
					<title>Courses Index</title>
				</Head>
				<Form.Group controlId="search">
                    <Form.Control 
                        type="text" 
                        placeholder="Search Courses"
                        onChange={(e) => setTerm(e.target.value)}
                        required
                    />
                </Form.Group>
                {coursesList}
			</React.Fragment>
	)
}

/*

courses = [
	<Course />,
	<Course />,
	<Course />
]

*/

// props = properties

/*
Course {
	props: {
		key: "wdc001",
		course: {
			name: "PHP-Laravel",
			description: "Lorem ipsum..."
			price: 45000
		}
	}
}
*/